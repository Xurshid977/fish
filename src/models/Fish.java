package models;

import models.enums.Gender;

import java.sql.Timestamp;

public class Fish {

    private String name;
    private Gender gender;
    private Long lifeTime;
    public Fish() {
    }

    public Fish(String name, Gender gender, Long lifeTime) {
        this.name = name;
        this.gender = gender;
        this.lifeTime = lifeTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Long getLifeTime() {
        return lifeTime;
    }

    public void setLifeTime(Long lifeTime) {
        this.lifeTime = lifeTime;
    }

    @Override
    public String toString() {
        return "Fish{" +
                "name='" + name + '\'' +
                ", gender=" + gender +
                ", lifeTime=" + lifeTime +
                '}';
    }
}
