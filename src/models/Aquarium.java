package models;

import java.util.ArrayList;
import java.util.List;

public class Aquarium {
    private List<Fish> fish = new ArrayList<>();

    public Aquarium() {
    }

    @Override
    public String toString() {
        return "Aquarium{" +
                "fish=" + fish +
                '}';
    }

    public Aquarium(List<Fish> fish) {
        this.fish = fish;
    }

    public List<Fish> getFish() {
        return fish;
    }

    public synchronized void setFish(List<Fish> fish) {
        this.fish = fish;
    }

}
