import models.Aquarium;
import models.Fish;
import models.enums.Gender;

import javax.swing.*;
import java.sql.Timestamp;
import java.util.*;
import java.util.Timer;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import static org.fusesource.jansi.Ansi.Color.*;
import static org.fusesource.jansi.Ansi.ansi;

public class Main {
    public static void main(String[] args) {
        if (JOptionPane.showConfirmDialog(null, "Boshlash", "start", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION) {
            Res res = new Res();
            Aquarium aquarium = new Aquarium();
            Fish fish = new Fish("Otabaliq",Gender.MALE,(long)10000);
            Fish fish1 = new Fish("Onabaliq",Gender.FEMALE,(long)10000);
            List<Fish> fishList = new ArrayList<>();
            fishList.add(fish);
            fishList.add(fish1);
            aquarium.setFish(fishList);
            res.aquarium =aquarium;
            MeetingThread meetingThread = new MeetingThread();
            meetingThread.setRes(res);
            meetingThread.start();
        }
    }
}



class MeetingThread extends Thread {
    Res res;

    public void setRes(Res res) {
        this.res = res;
    }

    int bt = 0;

    public int getBt() {
        return bt;
    }

    public void setBt(int bt) {
        this.bt = bt;
    }

    @Override
    public void run() {
        int meetingTime = ThreadLocalRandom.current().nextInt(100, 1000);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                Fish fish = new Fish();
                Fish fish1 = new Fish();
                while (true){
                        int firstFishIndex = ThreadLocalRandom.current().nextInt(0, res.getAquarium().getFish().size());
                        int secondFishIndex= ThreadLocalRandom.current().nextInt(0, res.getAquarium().getFish().size());
                        if(firstFishIndex!=secondFishIndex){
                            fish = res.getAquarium().getFish().get(firstFishIndex);
                            fish1 = res.getAquarium().getFish().get(secondFishIndex);
                            break;
                        }
                    }
                    if(!fish.getGender().equals(fish1.getGender()))
                    {
                        res.meeting(fish,fish1);
                        BornThread bornThread = new BornThread();
                        bornThread.setRes(res);
                        bornThread.start();
                    }
                    setBt(getBt() + meetingTime);
            }
        }, meetingTime, meetingTime);
    }
}

class BornThread extends Thread{

    Res res;

    public void setRes(Res res) {
        this.res = res;
    }

    @Override
    public void run() {
        int bornCount = ThreadLocalRandom.current().nextInt(5, 20);
        for (int i = 0; i <bornCount ; i++) {
            int deadTime = ThreadLocalRandom.current().nextInt(10000, 50000);
            Fish fish = res.bornFish(deadTime);
            DeadThread deadThread = new DeadThread(fish);
            deadThread.setRes(res);
            deadThread.start();
        }
    }
}

//o'lim
class DeadThread extends Thread {
    private Fish fish;

    Res res;

    public DeadThread(Fish fish) {
        this.fish = fish;
    }

    public void setRes(Res res) {
        this.res = res;
    }

    @Override
    public void run() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                res.deadFish(fish);
            }
        }, fish.getLifeTime());
    }
}

class Res {
    Aquarium aquarium;

    public Aquarium getAquarium() {
        return aquarium;
    }

    public synchronized  void meeting(Fish fish,Fish fish1){
        System.out.println(ansi().eraseScreen().fg(GREEN).a("\n" +"Meeting: "+fish.getName()+" "+fish.getGender()+" x "+fish1.getName()+" "+fish1.getGender() + "\n").reset());
    }

    public synchronized void deadFish(Fish fish) {
        List<Fish> fishList = aquarium.getFish().stream().filter(fish1 -> !fish1.equals(fish)).collect(Collectors.toList());
        aquarium.setFish(fishList);
        System.out.println(ansi().eraseScreen().fg(RED).a("\n" + fish.getName() + "GENDER:" + fish.getGender() + " " + "dead in" + fish.getLifeTime() + " ms\n" +
                "ALLCOUNT:"+aquarium.getFish().size()+"\n").reset());
    }

    public synchronized Fish bornFish(int deadTime) {
        int gender = ThreadLocalRandom.current().nextInt(1, 3);
        UUID uuid = UUID.randomUUID();
        Fish fish = new Fish(uuid + "", gender == 1 ? Gender.MALE : Gender.FEMALE, (long) deadTime);
        if (aquarium != null) {
            aquarium.getFish().add(fish);
            System.out.println(aquarium.getFish().size() + " " + fish.getName() + " GENDER:" + fish.getGender() + " born\n" +
                    "ALLCOUNT:"+aquarium.getFish().size());
            if (aquarium.getFish().size() == 1000) {
                System.out.println("Aquarium filled");
                System.exit(0);
            }
        }
        return fish;
    }
}

